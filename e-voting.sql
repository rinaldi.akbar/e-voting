-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2021 at 11:38 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e-voting`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, 'Gubernur', 'Calon Gubernur dan Wakil Gubernur', '2021-11-10 21:15:33', '2021-11-10 21:15:33'),
(2, 'DPRD', 'Calon Anggota DPRD', '2021-11-10 21:34:46', '2021-11-10 21:34:46'),
(3, 'DPR', 'Calon Anggota DPR', '2021-11-10 21:36:13', '2021-11-10 21:36:13'),
(4, 'DPD', 'Calon Anggota DPD', '2021-11-10 21:36:51', '2021-11-10 21:36:51'),
(5, 'Presiden', 'Calon Presiden dan Wakil Presiden', '2021-11-10 21:37:09', '2021-11-10 21:37:09');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_11_10_065426_create_kategori_table', 1),
(5, '2021_11_10_111937_create_pemilu_table', 1),
(6, '2021_11_10_122721_create_profile_table', 1),
(7, '2021_11_10_123832_create_voting_table', 1),
(8, '2021_11_10_125208_create_rekapitulasi_table', 1),
(9, '2021_11_11_032045_create_profile_table', 2),
(10, '2021_11_11_193104_create_profile_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pemilu`
--

CREATE TABLE `pemilu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pemilu`
--

INSERT INTO `pemilu` (`id`, `nama`, `foto`, `kategori_id`, `created_at`, `updated_at`) VALUES
(1, 'H. IIE SUMIRAT SUNDANA', '1636650175.jpg', 1, '2021-11-11 08:36:31', '2021-11-11 10:02:55'),
(3, 'ABDUL AZIZ ABDUL RAUF', '1636647580.jpg', 1, '2021-11-11 09:19:40', '2021-11-11 09:19:40'),
(4, 'NURMANSJAH LUBIS, SE, Ak, MM', '1636651986.jpg', 1, '2021-11-11 10:33:06', '2021-11-11 10:33:06'),
(6, 'HERMANTO, SE, MM', '1636738675.jpg', 1, '2021-11-12 10:37:55', '2021-11-12 10:37:55'),
(7, 'Drs. H. NURFIRMANWANSYAH, MM, Apt', '1636738780.jpg', 2, '2021-11-12 10:39:40', '2021-11-12 10:39:40'),
(8, 'HAMDANUS, S.Fil.I, M.Si', '1636738841.jpg', 2, '2021-11-12 10:40:41', '2021-11-12 10:40:41'),
(9, 'SYAHRIL JAMARIS, S.Si, Apt', '1636738864.jpg', 2, '2021-11-12 10:41:04', '2021-11-12 10:41:04'),
(10, 'ELYA DEFITA, S.Pd', '1636738889.jpg', 2, '2021-11-12 10:41:29', '2021-11-12 10:41:29');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `umur` int(11) NOT NULL,
  `jenis_kelamin` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `peran` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `alamat`, `umur`, `jenis_kelamin`, `peran`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Tambun Bekasi', 30, 'perempuan', 'pemilih', 2, '2021-11-11 12:42:17', '2021-11-11 13:30:31'),
(2, 'Depok Jawa Barat', 30, 'laki_laki', 'Admin', 1, '2021-11-11 17:00:00', '2021-11-11 17:00:00'),
(3, 'jakarta', 20, 'laki_laki', 'pemilih', 3, '2021-11-12 09:44:42', '2021-11-12 09:44:42'),
(4, 'semarang', 25, 'laki_laki', 'pemilih', 4, '2021-11-12 10:37:03', '2021-11-12 10:37:03');

-- --------------------------------------------------------

--
-- Table structure for table `rekapitulasi`
--

CREATE TABLE `rekapitulasi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `visibility` datetime NOT NULL,
  `pemilu_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Rinaldi Akbar Setiawan', 'rinaldiakbar.setiawan@gmail.com', NULL, '$2y$10$L0vBtu7LSccjl/ODIgiMpeaPG9yvS0dG43TiUS2.yO5zq5KWu/7CO', 'nsYGhu3YlU0J62HrtX57fGxt9xX75i8phKkbiBYfiwl3qpUngGo9CJl16Bls', '2021-11-11 11:57:05', '2021-11-11 11:57:05'),
(2, 'Rumaisa Ramadhani', 'rumaisharamadhani@gmail.com', NULL, '$2y$10$eGFRIvXORZng3ilzdDIp1ufNUVIuPMU7073nt6lHnIDdRMcfxnYCC', 'ItpzX2qjJxqhrT70gYjkckPFrhrokuwSzHFUStv3hs6YgV7ZbtkftBgdL2ed', '2021-11-11 12:42:17', '2021-11-11 12:42:17'),
(3, 'aa', 'aa@gmail.com', NULL, '$2y$10$1tQDUOcS5N268REtEza8meDpqpVI.X72GmQr6gBH74N.m.DIhNnKy', NULL, '2021-11-12 09:44:42', '2021-11-12 09:44:42'),
(4, 'bb', 'bb@gmail.com', NULL, '$2y$10$PIbayxuR0lN/DSLM4btOy.6ygPbdx7qOFEnHZaXyIsFT6FZDww7AG', NULL, '2021-11-12 10:37:03', '2021-11-12 10:37:03');

-- --------------------------------------------------------

--
-- Table structure for table `voting`
--

CREATE TABLE `voting` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `pemilu_id` bigint(20) UNSIGNED NOT NULL,
  `voting` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `voting`
--

INSERT INTO `voting` (`id`, `user_id`, `pemilu_id`, `voting`, `created_at`, `updated_at`) VALUES
(1, 3, 6, 1, '2021-11-12 11:09:43', '2021-11-12 11:09:43'),
(2, 3, 10, 1, '2021-11-12 11:09:46', '2021-11-12 11:09:46'),
(3, 3, 6, 1, '2021-11-12 11:25:03', '2021-11-12 11:25:03'),
(4, 2, 6, 1, '2021-11-13 08:31:35', '2021-11-13 08:31:35'),
(5, 2, 10, 1, '2021-11-13 08:31:39', '2021-11-13 08:31:39'),
(6, 2, 6, 1, '2021-11-13 08:34:56', '2021-11-13 08:34:56'),
(7, 2, 10, 1, '2021-11-13 08:34:58', '2021-11-13 08:34:58'),
(8, 2, 10, 1, '2021-11-13 08:36:48', '2021-11-13 08:36:48'),
(9, 2, 10, 1, '2021-11-13 08:36:56', '2021-11-13 08:36:56'),
(10, 2, 10, 1, '2021-11-13 08:37:14', '2021-11-13 08:37:14'),
(11, 2, 6, 1, '2021-11-13 08:38:33', '2021-11-13 08:38:33'),
(12, 2, 10, 1, '2021-11-13 08:38:37', '2021-11-13 08:38:37'),
(13, 2, 6, 1, '2021-11-13 08:39:29', '2021-11-13 08:39:29'),
(14, 2, 10, 1, '2021-11-13 08:39:32', '2021-11-13 08:39:32'),
(15, 2, 6, 1, '2021-11-13 08:40:19', '2021-11-13 08:40:19'),
(16, 2, 10, 1, '2021-11-13 08:40:21', '2021-11-13 08:40:21'),
(17, 2, 6, 1, '2021-11-13 08:42:05', '2021-11-13 08:42:05'),
(18, 2, 10, 1, '2021-11-13 08:42:07', '2021-11-13 08:42:07'),
(19, 2, 6, 1, '2021-11-13 08:42:40', '2021-11-13 08:42:40'),
(20, 2, 10, 1, '2021-11-13 08:42:43', '2021-11-13 08:42:43'),
(21, 2, 6, 1, '2021-11-13 08:43:53', '2021-11-13 08:43:53'),
(22, 2, 10, 1, '2021-11-13 08:44:04', '2021-11-13 08:44:04'),
(23, 2, 10, 1, '2021-11-13 08:44:15', '2021-11-13 08:44:15'),
(24, 2, 6, 1, '2021-11-13 08:44:33', '2021-11-13 08:44:33'),
(25, 2, 10, 1, '2021-11-13 08:44:35', '2021-11-13 08:44:35'),
(26, 2, 6, 1, '2021-11-13 08:45:29', '2021-11-13 08:45:29'),
(27, 2, 10, 1, '2021-11-13 08:45:31', '2021-11-13 08:45:31'),
(28, 2, 6, 1, '2021-11-13 08:46:08', '2021-11-13 08:46:08'),
(29, 2, 10, 1, '2021-11-13 08:46:10', '2021-11-13 08:46:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pemilu`
--
ALTER TABLE `pemilu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pemilu_kategori_id_foreign` (`kategori_id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profile_user_id_foreign` (`user_id`);

--
-- Indexes for table `rekapitulasi`
--
ALTER TABLE `rekapitulasi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rekapitulasi_pemilu_id_foreign` (`pemilu_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `voting`
--
ALTER TABLE `voting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `voting_user_id_foreign` (`user_id`),
  ADD KEY `voting_pemilu_id_foreign` (`pemilu_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pemilu`
--
ALTER TABLE `pemilu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `rekapitulasi`
--
ALTER TABLE `rekapitulasi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `voting`
--
ALTER TABLE `voting`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pemilu`
--
ALTER TABLE `pemilu`
  ADD CONSTRAINT `pemilu_kategori_id_foreign` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`);

--
-- Constraints for table `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `profile_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `rekapitulasi`
--
ALTER TABLE `rekapitulasi`
  ADD CONSTRAINT `rekapitulasi_pemilu_id_foreign` FOREIGN KEY (`pemilu_id`) REFERENCES `pemilu` (`id`);

--
-- Constraints for table `voting`
--
ALTER TABLE `voting`
  ADD CONSTRAINT `voting_pemilu_id_foreign` FOREIGN KEY (`pemilu_id`) REFERENCES `pemilu` (`id`),
  ADD CONSTRAINT `voting_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
