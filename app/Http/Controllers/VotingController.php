<?php

namespace App\Http\Controllers;

use DB;
use App\Voting;
use Auth;
use Illuminate\Http\Request;

class VotingController extends Controller
{
    public function vote($id)
    {
		$pemilu = DB::table('pemilu')->where('kategori_id', $id)->get();
		$kategori = DB::table('kategori')->where('id', $id)->first();
		return view('voting.create', compact('pemilu','kategori'));
    }
	
	public function store(Request $request)
    {
        $voting = new Voting();
		$voting->user_id = Auth::user()->id;
		$voting->pemilu_id = $request->pemilu_id;
		$voting->voting = 1;
		$kategori = $request->kategori;
		$next_kategori=intval($kategori)+1;
		$voting->save();
		if($next_kategori>2) return view('voting.penutup');
		else return redirect('/voting/'.$next_kategori.'/vote');
    }

	public function rekapitulasi()
	{
		$result_1 = DB::table('voting')
					->join('pemilu', 'pemilu_id','=','pemilu.id')
					->join('kategori', 'kategori_id','=','kategori.id')
					->where('kategori.id', '=', 1)
					->select('pemilu.nama', 'pemilu.kategori_id')
					->first();
		
		//select('select b.nama, kategori_id, deskripsi, pemilu_id, sum(voting) from voting a left join pemilu b on a.pemilu_id=b.id left join kategori c on kategori_id=c.id where kategori_id=2 group by b.nama, kategori_id, deskripsi, pemilu_id')->get();
		//dd($result_1);
		return view('voting.rekapitulasi', compact('result_1'));
	}
}
