<?php

namespace App\Http\Controllers;

use App\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
	public function index()
    {
		$kategori = Kategori::all();
		$title="Kategori";
		return view('kategori.index', compact('kategori','title'));
	}
	
	public function create()
    {
		$title="Kategori";
        return view('kategori.create', compact('title'));
    }
	
	public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required'
        ]);
        Kategori::create([
			"nama" => $request["nama"],
            "deskripsi" => $request["deskripsi"]
        ]);
        return redirect('/kategori');
    }
	
	public function edit($id)
    {
		$kategori = Kategori::findOrFail($id);
		return view('kategori.edit', compact('kategori','kategori'));
    }
	
	public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required'
        ]);
		
		$kategori=kategori::find($id);
		$kategori->nama = $request->nama;
		$kategori->deskripsi = $request->deskripsi;
		
		$kategori->save();
		
		return redirect('/kategori');
    }
	
	public function destroy($id)
	{
		$kategori=kategori::find($id);
		$kategori->delete();
		return redirect('/kategori');
	}
}

