<?php

namespace App\Http\Controllers;

use Auth;
use App\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
	{
		$profile = Profile::find(Auth::user()->id);
		return view('profile.edit', compact('profile'));
	}

    public function edit($id)
	{
		$profile = Profile::find($id);
		return view('profile.edit', compact('profile'));
	}
	
	public function update(Request $request, $id)
    {
        $request->validate([
            'alamat' => 'required',
            'umur' => 'required',
			'jenis_kelamin' => 'required'
        ]);
        $profile=Profile::find($id);
		$profile->alamat = $request->alamat;
		$profile->umur = $request->umur;
		$profile->jenis_kelamin = $request->jenis_kelamin;
        $profile->save();
        return redirect('/profile/'.$id.'/edit');
    }
}
