@extends('app')

@section('judul')
Halaman Rekapitulasi Bupati
@endsection

@section('content')

@push('style')

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.1/Chart.min.js"></script>

<style>

.chart{
    height:500px;
    width:500px;
}
.pie-legend {
	list-style: none;
	margin: 0;
	padding: 0;
}
.pie-legend span {
	display: inline-block;
	width: 14px;
	height: 14px;
	border-radius: 100%;
	margin-right: 16px;
	margin-bottom: -2px;
}
.pie-legend li {
	margin-bottom: 10px;
}
</style>
@endpush



<div class="col-md-5 my-5 mx-auto">
    <div class="card text-center shadow">
        <div class="card-header text-left"><b>{{$result_1->nama}}</b> - Jumlah Pemilih</div>
        <div class="card-body">
            <div class="chart">
            	<canvas id="property_types" class="pie"></canvas>
            	<div id="pie_legend" class="py-3 text-left col-md-7 mx-auto"></div>
            </div>
        </div>
    </div>
</div>

<script>

	// global options variable
	var options = {
		responsive: true,
		easing:'easeInExpo',
		scaleBeginAtZero: true,
        // you don't have to define this here, it exists inside the global defaults
		legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
	}

		// PIE
		// PROPERTY TYPE DISTRIBUTION
		// context
		var ctxPTD = $("#property_types").get(0).getContext("2d");
		// data
		var dataPTD = [
			{
				label: "Single Family Residence",
				color: "#5093ce",
				highlight: "#78acd9",
				value: 52
			},
			{
				label: "Townhouse/Condo",
				color: "#c7ccd1",
				highlight: "#e3e6e8",
				value: 12
			},
			{
				label: "Land",
				color: "#7fc77f",
				highlight: "#a3d7a3",
				value: 6
			},
			{
				label: "Multifamily",
				color: "#fab657",
				highlight: "#fbcb88",
				value: 8
			},
			{
				label: "Farm/Ranch",
				color: "#eaaede",
				highlight: "#f5d6ef",
				value: 8
			},
			{
				label: "Commercial",
				color: "#dd6864",
				highlight: "#e6918e",
				value: 14
			},
			
		]

		// Property Type Distribution
		var propertyTypes = new Chart(ctxPTD).Pie(dataPTD, options);
			// pie chart legend
			$("#pie_legend").html(propertyTypes.generateLegend());




</script>

@endsection