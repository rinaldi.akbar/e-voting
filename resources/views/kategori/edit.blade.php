@extends('app')

@section('content')
<div class="card">
	<div class="card-header">
		<h3 class="card-title">Edit Kategori</h3>

		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
				<i class="fas fa-minus"></i>
			</button>
			<button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
				<i class="fas fa-times"></i>
			</button>
		</div>
	</div>
	<div class="card-body">
		<div>
			<form action="/kategori/{{$kategori->id}}" method="POST">
				@csrf
				@method('PUT')
				<div class="form-group">
					<label for="nama">Nama</label>
					<input type="text" class="form-control" name="nama" id="nama" value="{{$kategori->nama}}">
					@error('nama')
					<div class="alert alert-danger">
						{{ $message }}
					</div>
					@enderror
				</div>
				<div class="form-group">
					<label for="deskripsi">Deskripsi</label>
					<input type="text" class="form-control" name="deskripsi" id="deskripsi" value="{{$kategori->deskripsi}}">
					@error('deskripsi')
					<div class="alert alert-danger">
						{{ $message }}
					</div>
					@enderror
				</div>
				<button type="submit" class="btn btn-primary">Edit</button>
			</form>
		</div>
	</div>
	<!-- /.card-body -->
	<div class="card-footer">
		Final Project 1
	</div>
	<!-- /.card-footer-->
</div>
@endsection