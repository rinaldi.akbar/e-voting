@extends('app')

@push('style')
<style type="text/css">
    .card {
        margin-top: 2em;
        padding: 1.5em 0.5em 0.5em;
        border-radius: 2em;
        text-align: center;
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    }

    .card img {
        width: 65%;
        border-radius: 50%;
        margin: 0 auto;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
    }

    .card .card-title {
        font-weight: 700;
        font-size: 1.5em;
    }
</style>
@endpush

@section('content')
@foreach ($kategori as $item)
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$item->deskripsi}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <a href="/pemilu/create" class="btn btn-primary">Tambah</a>
        <br><br>
        <form action="/voting/" method="POST">
            @csrf
            <ul class="row">
                @foreach($pemilu as $value)
                @if($value->kategori_id==$item->id)
                <li class="col-12 col-md-6 col-lg-3">
                    <!--<div class="cnt-block equal-hight" style="height: 349px;">
                        <figure><img src="{{asset('data_file/'.$value->foto)}}" class="img-responsive" alt=""></figure>

                        <h3><br>{{$value->nama}}</h3>
                        <ul class="follow-us clearfix">
                            <li>
                                <a href="/pemilu/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
                            </li>
                        </ul>
                    </div>-->

                    <div class="card" style="width: 18rem;">
                        <img src="{{asset('data_file/'.$value->foto)}}" class="card-img-top" alt="">
                        <div class="card-body">
                            <h5 class="card-title">{{$value->nama}}</h5>
                            <p class="card-text">{{$value->kategori->deskripsi}}</p>
                            <a href="/pemilu/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
                        </div>
                    </div>
                </li>
                @endif
                @endforeach
            </ul>
        </form>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
    {{$item->deskripsi}}
    </div>
    <!-- /.card-footer-->
</div>
@endforeach
@endsection